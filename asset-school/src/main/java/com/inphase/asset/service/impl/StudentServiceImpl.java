package com.inphase.asset.service.impl;

import com.github.pagehelper.PageHelper;
import com.inphase.asset.dao.StudentDao;
import com.inphase.asset.domain.StudentDto;
import com.inphase.asset.entity.Student;
import com.inphase.asset.entity.meta.StudentMeta;
import com.inphase.asset.service.StudentService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

/**
 *  Service Impl
 * @author xiaob@inphase.com
 * @date 2017-09-21 21:05:51
 * @since 1.0
 * @version 1.0
 */
@Service
public class StudentServiceImpl implements StudentService {
    protected static final Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Autowired
    StudentDao studentDao;

    public StudentDto addStudent(StudentDto studentDto) {
        //todo Do something
        return null;
    }

    public int deleteStudent(String ids) {
        //todo Do something
        return 0; 
    }

    public StudentDto updateStudent(StudentDto studentDto) {
        //todo Do something
        return null;
    }

    public List<StudentDto> findStudent(String ids) {


        List<Integer> integers = new ArrayList<>();

        Arrays.asList(ids.split(",")).stream().mapToInt(t -> Integer.valueOf(t)).forEach(id ->{
            integers.add(id);
        });

        if(integers.isEmpty()){
            return Collections.emptyList();
        }

        Example example = new Example(Student.class);
        example.createCriteria().andIn(StudentMeta.id,integers);
        return studentDao.selectDtoByExample(example,StudentDto.class);
    }
}
DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `psw` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `student` (name,psw) VALUES ( '123', '123');
INSERT INTO `student` (name,psw) VALUES ( '234', '234');
INSERT INTO `student` (name,psw) VALUES ('22', '22');
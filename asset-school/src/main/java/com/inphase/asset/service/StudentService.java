package com.inphase.asset.service;

import com.inphase.asset.domain.StudentDto;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *  Service
 * @author xiaob@inphase.com
 * @date 2017-09-21 21:05:51
 * @since 1.0
 * @version 1.0
 */
@Transactional
public interface StudentService {
    /**
     * Add
     * @param studentDto
     * @return
     */
    StudentDto addStudent(StudentDto studentDto);

    /**
     * Delete
     * @param ids
     * @return
     */
    int deleteStudent(String ids);

    /**
     * Update
     * @param studentDto
     * @return
     */
    StudentDto updateStudent(StudentDto studentDto);

    /**
     * find
     * @param ids
     * @return
     */
    List<StudentDto> findStudent(String ids);
}
package com.inphase.asset.configure;

//import org.mybatis.spring.mapper.MapperScannerConfigurer;


import com.x.mapper.example.SelectDtoByExampleMapper;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import tk.mybatis.mapper.code.Style;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.spring.mapper.MapperScannerConfigurer;

/**
 *  注意，由于MapperScannerConfigurer执行的比较早，所以必须有下面的注解
 * @author yehc
 * @date 16/12/07
 *
 */
@Configuration
//@AutoConfigureAfter(MyBatisConfigurer.class)
@Order(Ordered.HIGHEST_PRECEDENCE+1)
public class MyBatisMapperScannerConfigurer {

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
        mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
        mapperScannerConfigurer.setBasePackage("com.inphase.asset.dao");

        mapperScannerConfigurer.getMapperHelper().registerMapper(SelectDtoByExampleMapper.class);
        mapperScannerConfigurer.getMapperHelper().registerMapper(Mapper.class);
//        用于实体和数据库（让驼峰转下划线）jpa为注解字段映射
        mapperScannerConfigurer.getMapperHelper().getConfig().setStyle(Style.camelhump);
        return mapperScannerConfigurer;
    }


}

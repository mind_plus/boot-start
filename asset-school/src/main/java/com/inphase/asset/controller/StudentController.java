package com.inphase.asset.controller;

import com.inphase.asset.domain.StudentDto;
import com.inphase.asset.service.StudentService;
import com.inphase.utils.Response;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *  Controller
 * @author xiaob@inphase.com
 * @date 2017-09-21 21:05:51
 * @since 1.0
 * @version 1.0
 */
@RestController
public class StudentController {
    protected static final Logger logger = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    protected StudentService studentService;

    /**
     * 
     * @param studentDto
     * @param result
     * @return
     */
    @RequestMapping(value = "/students", method = RequestMethod.POST)
    public Response addStudent(@Valid @RequestBody StudentDto studentDto, BindingResult result) {
        if (result.hasErrors()) {
            return new Response().failure();
        }
        studentDto = studentService.addStudent(studentDto);
        return new Response().success(studentDto);
    }

    /**
     * 
     * @param ids
     * @return
     */
    @RequestMapping(value = "/students/{ids}", method = RequestMethod.DELETE)
    public Response deleteStudent(@PathVariable String ids) {
        Integer number = studentService.deleteStudent(ids);
        return new Response().success(number);
    }

    /**
     * 
     * @param id
     * @param studentDto
     * @param result
     * @return
     */
    @RequestMapping(value = "/students/{id}", method = RequestMethod.PATCH)
    public Response updateStudent(@PathVariable Long id, @Valid @RequestBody StudentDto studentDto, BindingResult result) {
        if (result.hasErrors()) {
            return new Response().failure();
        }
        studentDto = studentService.updateStudent(studentDto);
        return new Response().success(studentDto);
    }

    /**
     * 
     * @param ids
     * @return
     */
    @RequestMapping(value = "/students/{ids}", method = RequestMethod.GET)
    public Response findStudent(@PathVariable String ids) {
        List<StudentDto> dtos = studentService.findStudent(ids);
        return new Response().success(dtos);
    }
}
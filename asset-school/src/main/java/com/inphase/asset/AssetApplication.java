package com.inphase.asset;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xiaob@inphase.com
 * @date 2017/9/21
 */

@SpringBootApplication
public class AssetApplication {

    public static void main(String[] args) {

        SpringApplication.run(AssetApplication.class,args);
    }
}

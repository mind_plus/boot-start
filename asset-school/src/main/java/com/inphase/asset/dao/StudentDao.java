package com.inphase.asset.dao;

import com.inphase.asset.entity.Student;
import com.x.mapper.BaseDao;

import java.util.List;

public interface StudentDao extends BaseDao<Student> {

    public List<Student> selectTest();
}
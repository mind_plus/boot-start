package com.inphase.asset.configure;

import com.inphase.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 异常处理
 */

public interface ExceptionControllerAdvice<T> {

    Logger log = LoggerFactory.getLogger(ExceptionControllerAdvice.class);

    @ExceptionHandler(Throwable.class)
    default T handler(Throwable e){
        if(log.isDebugEnabled()){
            log.debug("请求出错了,",e);
        }
       return (T) "error";
    }

    @ControllerAdvice(annotations = {Controller.class})
    public static class ExceptionView implements ExceptionControllerAdvice<String>{
    }

    @RestControllerAdvice(annotations = {RestController.class})
    public static class ExceptionData implements ExceptionControllerAdvice<Response>{
        @Override
        public Response handler(Throwable e) {
            if(log.isDebugEnabled()){
                log.debug("请求出错了,",e);
            }
            return new Response().failure("error");
        }
    }

}


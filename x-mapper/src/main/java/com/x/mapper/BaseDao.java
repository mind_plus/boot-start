package com.x.mapper;

import com.x.mapper.example.SelectDtoByExampleMapper;
import tk.mybatis.mapper.common.Mapper;

/**
 * 基本DAO提供，Mapper扩展
 * @author 252944454@qq.com
 * @date 2016/12/16
 */
public interface BaseDao<T> extends Mapper<T>,SelectDtoByExampleMapper<T> {
}

package com.inphase.asset.service.impl;


import com.inphase.asset.dao.StudentDao;
import com.inphase.asset.entity.Student;
import com.inphase.asset.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 * @author xiaob@inphase.com
 * @date 2017/9/21
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentServiceImplTest {

    @Autowired
    StudentService service;

    @Autowired
    StudentDao studentDao;



    @Test
    public void findStudent() throws Exception {
      //  service.findStudent("1,2,3");
        Student student = new Student();
        student.setName("name");
        student.setPsw("pwd");
        int result = studentDao.insert(student);

        Assert.notNull(student.getId(),"不能返回ID");
    }

    @Test
    public void findStudent2() throws Exception {
        studentDao.selectTest();


    }

}
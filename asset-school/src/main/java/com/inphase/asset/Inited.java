package com.inphase.asset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author xiaob@inphase.com
 * @date 2017/9/21
 */
@Component
public class Inited  implements CommandLineRunner {

    public  static Logger logger = LoggerFactory.getLogger(Inited.class);



    @Override
    public void run(String... args) throws Exception {
        logger.info("程序加载完毕.......");
    }
}

package com.inphase.asset.controller;

import org.springframework.stereotype.Controller;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author xiaob@inphase.com
 * @date 2017/9/21
 */

@Controller
public class HomeController {

    @RequestMapping({"/","index"})
    public String home(){
        return "index";
    }

    /**
     * 按视图路径解析视图
     * @param request
     * @return
     */
    @RequestMapping("/view/**")
    public  String viewPath(HttpServletRequest request){


        String pattern = (String)
                request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);

        String path = new AntPathMatcher().extractPathWithinPattern(pattern,
                request.getServletPath());


        return path;
    }
}

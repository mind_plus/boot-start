package com.inphase.utils;

/**
 * @author xiaob@inphase.com
 * @date 2017/9/21
 */
public class Response<T> {
    private Response<T>.Meta meta;
    private T data;

    public Response() {
    }

    public Boolean checkState() {
        return Boolean.valueOf(this.meta.getCode() == ResponseState.SUCCESS.getCode());
    }

    public Response success() {
        this.meta = new Response.Meta(ResponseState.SUCCESS);
        return this;
    }

    public Response success(T data) {
        this.success();
        this.data = data;
        return this;
    }

    public Response failure() {
        this.meta = new Response.Meta(ResponseState.FAIL);
        return this;
    }

    public Response failure(String message) {
        this.meta = new Response.Meta(ResponseState.FAIL, message);
        return this;
    }

    public Response failure(T data, String message) {
        this.failure(message);
        this.data = data;
        return this;
    }

    public Response<T>.Meta getMeta() {
        return this.meta;
    }

    public T getData() {
        return this.data;
    }

    public Response setData(T data) {
        this.data = data;
        return this;
    }

    public class Meta {
        private ResponseState state;
        private String message;

        public Meta() {
        }

        public Meta(ResponseState responseState) {
            this.state = responseState;
            this.message = responseState.getCodeInfo();
        }

        public Meta(ResponseState responseState, String message) {
            this.state = responseState;
            if(message == null) {
                message = responseState.getCodeInfo();
            }

            this.message = message;
        }

        public int getCode() {
            return this.state.getCode();
        }

        public void setCode(ResponseState responseState) {
            this.state = responseState;
        }

        public String getMessage() {
            return this.message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


    public enum ResponseState {
        FAIL(0, "失败"),
        SUCCESS(1, "成功");

        private int code;
        private String codeInfo;

        private ResponseState(int code, String codeInfo) {
            this.code = code;
            this.codeInfo = codeInfo;
        }

        public int getCode() {
            return this.code;
        }

        public String getCodeInfo() {
            return this.codeInfo;
        }

        public String toString() {
            return super.toString().toLowerCase();
        }

        public static ResponseState byCode(int code) {
            ResponseState[] states = values();

            for(int i = 0; i < states.length; ++i) {
                ResponseState state = states[i];
                if(state.getCode() == code) {
                    return state;
                }
            }

            return FAIL;
        }
    }
}
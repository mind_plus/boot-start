package com.x.mapper.example;



import com.x.mapper.annotations.DtoResult;
import com.x.mapper.provider.DtoExampleProvider;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * resultMap type基于Dto模型
 * @author 252944454@qq.com
 * @date 2016/12/14
 */
public interface SelectDtoByExampleMapper<T> {
    /**
     * 根据Example条件进行查询
     *
     * @param example
     * @return
     */
    @SelectProvider(type = DtoExampleProvider.class, method = "dynamicSQL")
    <E>List<E>  selectDtoByExample(Object example,@DtoResult Class<E> dtoCls);
}

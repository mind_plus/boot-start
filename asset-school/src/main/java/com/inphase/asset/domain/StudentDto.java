package com.inphase.asset.domain;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Dto
 * @author xiaob@inphase.com
 * @date 2017-09-21 21:05:51
 * @since 1.0
 * @version 1.0
 */
public class StudentDto {
    private Integer id;

    @NotEmpty(message = "{ student.dto.name.not.empty }")
    private String name;

    @NotEmpty(message = "{ student.dto.psw.not.empty }")
    private String psw;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public String getPsw() {
        return psw;
    }
}
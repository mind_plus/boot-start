package com.inphase.asset.entity.meta;

public final class StudentMeta {
    public static final String id = "id";

    public static final String name = "name";

    public static final String psw = "psw";
}